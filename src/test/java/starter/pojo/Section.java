package starter.pojo;

public class Section {
    private String activityTitle;
    private String activitySubtitle;
    private boolean markdown;
    private String text;

    // Constructor
    public Section(String activityTitle, String activitySubtitle, boolean markdown, String text) {
        this.activityTitle = activityTitle;
        this.activitySubtitle = activitySubtitle;
        this.markdown = markdown;
        this.text = text;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public String getActivitySubtitle() {
        return activitySubtitle;
    }

    public void setActivitySubtitle(String activitySubtitle) {
        this.activitySubtitle = activitySubtitle;
    }

    public boolean isMarkdown() {
        return markdown;
    }

    public void setMarkdown(boolean markdown) {
        this.markdown = markdown;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
