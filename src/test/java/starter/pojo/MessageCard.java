package starter.pojo;

import java.util.List;

public class MessageCard {
    private String type;
    private String context;
    private String themeColor;
    private String summary;
    private List<Section> sections;

    // Constructor
    public MessageCard(String type, String context, String themeColor, String summary, List<Section> sections) {
        this.type = type;
        this.context = context;
        this.themeColor = themeColor;
        this.summary = summary;
        this.sections = sections;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getThemeColor() {
        return themeColor;
    }

    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }
}
