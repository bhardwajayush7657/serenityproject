package starter.search;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
public class teamsNotify {
    public static void main(String[] args) throws FileNotFoundException {
        String webhookUrl = "https://kronos.webhook.office.com/webhookb2/67de1bc8-4b3a-408d-9e31-87872859eac9@7b6f35d2-1f98-4e5e-82eb-e78f6ea5a1de/IncomingWebhook/705391d51a1e410eb341e3f37012b628/b6031e2e-a96e-4cc1-93c5-bada297ec45b";

        // Define the path to your Cucumber JSON report
        String reportPath = "C:\\serenity-cucumber-starter\\target\\cucumber-reports\\Cucumber.json";

        // Read the report file
        String reportData = new Scanner(new File(reportPath)).useDelimiter("\\Z").next();

        // Parse the report JSON
        Gson gson = new Gson();
        JsonObject reportJson = gson.fromJson(reportData, JsonObject.class);
        JsonArray elementsJson = reportJson.getAsJsonArray("elements");

        // Extract the relevant data from the report
        //String featureName = reportJson.get("name").getAsString();
        String featureName = elementsJson.get(0).getAsJsonObject().get("name").getAsString();
        System.out.println(featureName);

        int scenarioCount = elementsJson.size();
        System.out.println(scenarioCount);
        int passCount = 0;
        int failCount = 0;
        int skipCount = 0;
        for (JsonElement elementJson : elementsJson) {
            JsonObject resultJson = elementJson.getAsJsonObject().get("result").getAsJsonObject();
            String status = resultJson.get("status").getAsString();
            if (status.equals("passed")) {
                passCount++;
            } else if (status.equals("failed")) {
                failCount++;
            } else if (status.equals("skipped")) {
                skipCount++;
            }
        }

        // Create the message card payload
        JsonObject messageJson = new JsonObject();
        messageJson.addProperty("@type", "MessageCard");
        messageJson.addProperty("@context", "http://schema.org/extensions");
        messageJson.addProperty("themeColor", "0076D7");
        messageJson.addProperty("summary", "Cucumber report for ✅" + featureName);

        // Add the message card sections
        JsonArray sectionsJson = new JsonArray();

        // Add the feature section
        JsonObject featureSectionJson = new JsonObject();
        featureSectionJson.addProperty("activityTitle", "Feature: " + featureName);
        featureSectionJson.addProperty("activitySubtitle", "<strong>" + scenarioCount + " Scenarios</strong>");
        featureSectionJson.addProperty("act     ivityImage", "https://i.imgur.com/rhnd2ew.png");
        featureSectionJson.addProperty("markdown", "true");

        // Add the scenarios section
        JsonObject scenariosSectionJson = new JsonObject();
        scenariosSectionJson.addProperty("title", "<strong>Scenarios</strong>");
        scenariosSectionJson.addProperty("markdown", "true");
        StringBuilder scenariosTableBuilder = new StringBuilder();
        scenariosTableBuilder.append("<table>");
        scenariosTableBuilder.append("<tr><th>Scenario</th><th>Status</th></tr>");
        for (JsonElement elementJson : elementsJson) {
            JsonObject element = elementJson.getAsJsonObject();
            String scenarioName = element.get("name").getAsString();
            JsonObject resultJson = element.get("result").getAsJsonObject();
            String status = resultJson.get("status").getAsString();
            String statusIcon = "";
            String statusColor = "";
            if (status.equals("passed")) {
                statusIcon = "<img src=\"https://i.imgur.com/Kxj6LOg.png\" width=\"16\" height=\"16\"/>";
                statusColor = "green";
            } else if (status.equals("failed")) {

            }
}}}
