package starter.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class processBuilder {
    public static void main(String[] args) throws IOException, InterruptedException {
        String pythonInterpreter = "python";
        String pythonScriptPath = "C:\\serenity-cucumber-starter\\src\\test\\resources\\features\\search\\sharepoint.py";

        // Create the ProcessBuilder instance with the Python interpreter and script path
        ProcessBuilder processBuilder = new ProcessBuilder(pythonInterpreter, pythonScriptPath);

        // Start the process
        Process process = processBuilder.start();

        // Get the input stream of the process
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;

        // Read the output of the Python script
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

        // Wait for the process to complete
        int exitCode = process.waitFor();
        System.out.println("Python script exited with code: " + exitCode);

    }
}
