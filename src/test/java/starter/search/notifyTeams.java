package starter.search;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class notifyTeams {
    public static void main(String[] args) throws FileNotFoundException {
        String webhookUrl = "https://kronos.webhook.office.com/webhookb2/67de1bc8-4b3a-408d-9e31-87872859eac9@7b6f35d2-1f98-4e5e-82eb-e78f6ea5a1de/IncomingWebhook/705391d51a1e410eb341e3f37012b628/b6031e2e-a96e-4cc1-93c5-bada297ec45b";
        String reportPath = "C:\\serenity-cucumber-starter\\target\\cucumber-reports\\Cucumber.json";
        String reportData = new Scanner(new File(reportPath)).useDelimiter("\\Z").next();
        Gson gson = new Gson();
        JsonArray jsonArray=gson.fromJson(reportData,JsonArray.class);
        int pass=0;
        int fail=0;
        int skip=0;

        for(JsonElement kj:jsonArray){
            String featureName=kj.getAsJsonObject().get("name").getAsString();

            JsonArray elements=kj.getAsJsonObject().get("elements").getAsJsonArray();
            for(JsonElement jg:elements){
                String scenarioName=jg.getAsJsonObject().get("name").getAsString();
                JsonArray stepsArray=jg.getAsJsonObject().get("steps").getAsJsonArray();
                List<String> status=new ArrayList<>();
                for(JsonElement step:stepsArray){
                  status.add(step.getAsJsonObject().get("result").getAsJsonObject().get("status").getAsString());
                }
                if (status.contains("passed")){
                    pass++;
                }
            }

        }


    }
}
