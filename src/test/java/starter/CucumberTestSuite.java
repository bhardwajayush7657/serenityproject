package starter;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = { "pretty", "junit:target/cucumber-reports/Cucumber.xml","json:target/cucumber-reports/Cucumber.json","testng:target/cucumber-reports/testng.xml" },
        monochrome = true,
        features = "src/test/resources/features",
        glue = {"starter.stepdefinitions"},
        tags = ""

)
public class CucumberTestSuite {}
