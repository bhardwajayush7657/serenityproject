import os
import json
import requests

# Load cucumber.json report
with open('C:\\serenity-cucumber-starter\\target\\cucumber-reports\\Cucumber.json', 'r') as file:
    report = json.load(file)

# Extract relevant information from the report
# Send the card to Microsoft Teams channel
webhook_url = 'https://kronos.webhook.office.com/webhookb2/67de1bc8-4b3a-408d-9e31-87872859eac9@7b6f35d2-1f98-4e5e-82eb-e78f6ea5a1de/IncomingWebhook/705391d51a1e410eb341e3f37012b628/b6031e2e-a96e-4cc1-93c5-bada297ec45b'

# Extract relevant information from the report
failed_scenarios = []
for feature in report:

    for element in feature['elements']:

        for step in element['steps']:

            if step['result']['status'] == 'passed':
                failed_scenario = {
                    'feature': feature['name'],
                    'scenario': element['name'],
                    'step': step['name']
                }
                failed_scenarios.append(failed_scenario)

# Prepare data for the Microsoft Teams card
card_title = 'Cucumber Test Report'
card_summary = 'Failed Scenarios'
card_text = ''
for scenario in failed_scenarios:
    card_text += f"✅ **Feature:** {scenario['feature']}\n"
    card_text += f"❌ **Scenario:** {scenario['scenario']}\n"
    card_text += f"❌ **Step:** {scenario['step']}\n\n"

card_payload = {
    "@type": "MessageCard",
    "@context": "https://schema.org/extensions",
    "themeColor": "FF0000",
    "summary": card_summary,
    "sections": [{
        "activityTitle": card_title,
        "activitySubtitle": card_summary,
        "markdown": True,
        "text": card_text
    }]
}

# Send the card to Microsoft Teams channel

response = requests.post(webhook_url, json=card_payload)
if response.status_code == 200:
    print('Card sent successfully to Microsoft Teams!')
else:
    print(f'Failed to send card to Microsoft Teams. Status code: {response.status_code}')
    print(response.text)