package starter;

import com.google.gson.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static io.restassured.RestAssured.given;

public class parseXml {


    public static Element getElementByTagName(Document document, String tagName) {
        NodeList nodeList = document.getElementsByTagName(tagName);
        if (nodeList.getLength() > 0) {
            return (Element) nodeList.item(0);
        } else {
            return null;
        }
    }
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        boolean sendData=true;
        if(sendData){
        Date date=new Date();
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
        System.out.println(date.toString());
        String Pass="<div style=\"color: green;\">&#9989</div>";
        String Fail="<div style=\"color: red;\">&#10060</div";
        String webhook_url = "https://kronos.webhook.office.com/webhookb2/67de1bc8-4b3a-408d-9e31-87872859eac9@7b6f35d2-1f98-4e5e-82eb-e78f6ea5a1de/IncomingWebhook/705391d51a1e410eb341e3f37012b628/b6031e2e-a96e-4cc1-93c5-bada297ec45b";
        int totalTests=0;
        int totalPass=0;
        int totalFail=0;
        DocumentBuilderFactory documentBuilderFactory=DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder=documentBuilderFactory.newDocumentBuilder();
        Document document=documentBuilder.parse("C:\\serenity-cucumber-starter\\target\\cucumber-reports\\testng.xml");
        // Normalize the document
        document.getDocumentElement().normalize();

        // Get the root element
        Element rootElement = document.getDocumentElement();
        //*************************************************************************************

        Element finalResults=getElementByTagName(document,"testng-results");
        if(finalResults!=null) {
            totalTests = Integer.parseInt(finalResults.getAttribute("total"));
            totalPass=Integer.parseInt(finalResults.getAttribute("passed"));
            totalFail=Integer.parseInt(finalResults.getAttribute("failed"));
        }
        //***************************************************************************
        NodeList testMethodNodes = document.getElementsByTagName("test-method");

        Map<String, String> testMethodStatusMap = new HashMap<>();

        for (int i = 0; i < testMethodNodes.getLength(); i++) {
            Element testMethodElement = (Element) testMethodNodes.item(i);
            String methodName = testMethodElement.getAttribute("name");
            String status = testMethodElement.getAttribute("status");

            testMethodStatusMap.put(methodName, status);
        }
        System.out.println(testMethodStatusMap);
        // Print the test method names and their status
        for (Map.Entry<String, String> entry : testMethodStatusMap.entrySet()) {
            System.out.println("Test Method Name: " + entry.getKey());
            System.out.println("Status: " + entry.getValue());
            System.out.println("------------");
        }

        //***************************************
        StringBuilder cardText= new StringBuilder();
        cardText.append("**Total Tests**: ").append(String.valueOf(totalTests)).append("\n");
        cardText.append("<div style=\"color: green;\">&#9989; <b>Passed: ").append(String.valueOf(totalPass)).append("</b></div>").append("\n");
        cardText.append("<div style=\"color: red;\">&#10060; <b>Failed: ").append(String.valueOf(totalFail)).append("</b></div>").append("\n");
        cardText.append("<div><b>**************************************************************************************</b></div>").append("\n");
        cardText.append("<div style=\"color: green;\"><b>Feature Results for </b></div>").append("\n");
        for (Map.Entry<String, String> entry : testMethodStatusMap.entrySet()) {
            cardText.append("<div><b>").append(entry.getValue()).append(" - ").append(entry.getKey()).append("</b></div>").append("\n\n");
        }
        Gson gson = new Gson();
        JsonObject payload = new JsonObject();
        payload.addProperty("@type", "MessageCard");
        payload.addProperty("@context", "http://schema.org/extensions");
        payload.addProperty("summary", "Execution Reports");
       // payload.addProperty("text", cardText);
        payload.addProperty("themeColor", "FF0000");
        JsonArray array=new JsonArray();
        //JsonElement section1=new JsonObject();
        String title = "<div style=\"color: green; font-size: 16px; font-weight: bold;\">Execution reports for "+simpleDateFormat.format(date)+"</div>" ;
        JsonObject section1 = new JsonObject();
        section1.addProperty("activityTitle", title);
        section1.addProperty("text", cardText.toString());
        payload.add("sections", gson.toJsonTree(new JsonObject[]{section1}));
        String json = gson.toJson(payload);
        given().when().body(json).post(webhook_url);
    }}
}
